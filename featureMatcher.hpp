
#ifndef FEATUREMATCHER_H
#define FEATUREMATCHER_H
//Feature extraction and matching.

#define USE_ORB_DETECTOR 1
#define MAX_HOMOGRAPHY_DISTANCE 250
#define KNN_RATIO 0.7

#include <opencv2/opencv.hpp>
#include <iostream>
#include "opencv2/features2d/features2d.hpp"
#include <vector>
#include <stdio.h>
#include <opencv2/nonfree/nonfree.hpp>
#include "ROIExtractor.hpp"
#include "TemplateImage.hpp"
#include "Pyramid.hpp"


using namespace cv;
using namespace std;

class featureMatcher{
public:
   //all the functions
   featureMatcher();
   ~featureMatcher();
   void setTemplate(Mat&);
   void frameProcess(Rect);
   void drawMatchFrame(string name);
   void setFrame(Mat&);
   void matchObjectScene(Pyramid * object, Mat& sceneData, vector< KeyPoint >& sceneKeypoints);
   

private:
   void assertValid();
   TemplateImage * _templ;
   Mat _match_frame;
   Mat _scene;
   Mat _sceneProcessed;

   // Good matches have a ratio between first and second match of less than 0.8
   vector< KeyPoint > _allSceneKeypoints;
   vector< DMatch > _good_matches;

};

void initDetectorExtractor();
void preProcess(Mat &input, Mat &output);
void getKeyPoints(Mat &input,  vector<KeyPoint>& output);
void computeData(Mat& t, Mat& data, vector< KeyPoint >& keypoints);
#endif
