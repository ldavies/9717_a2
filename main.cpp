
#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <dirent.h>
#include "featureMatcher.hpp"

using namespace cv;
using namespace std;

vector<featureMatcher *> matcher;
vector<string> temp_names;
namespace
{
  void help(char** av)
  {
      cout << "\nObject matcher in a video stream or sequence of frames.\n"
      "Usage:\n./" << av[0] << " -<d|v> <video device number|directory to images> <" << "q,Q,esc -- quit\n"
      << "space   -- save frame\n\n"
      << "\tThe program captures frames from a camera connected to your computer.\n"
      << "\tTo find the video device number, try ls /dev/video* \n"
      << "\tYou may also pass a video file, like my_vide.avi instead of a device number\n"
      << "\tLastly, by specifying the option -d a directory of images can be passed instead of a video."
      << "\n"
      << endl;
  }
   
   void setup(vector<Mat> &tmpl){
      //create matcher object
      for(int i=0;i<tmpl.size();i++){
         featureMatcher * f = new featureMatcher();
         matcher.push_back(f);
	 matcher[matcher.size()-1]->setTemplate(tmpl[i]);
      }
   }
   
   void process(Mat &frame){
      vector<Rect> rois;
      
      for(int i=0;i<matcher.size();i++){
         matcher[i]->setFrame(frame);
         matcher[i]->drawMatchFrame(temp_names[i]);
      }
   }
   
  /* Returns number of images
   * Pass string of base directory and pointer to an array of Mat
   */
  int getImages(const char * base, vector<Mat> * images) {
  
  #if false
    DIR *dir;
    struct dirent *ent;
    int num = 0;
    string d = string( base );
    if ((dir = opendir (base)) != NULL) {
  
      /* print all the files and directories within directory */
      while ((ent = readdir (dir)) != NULL) 
        {
  
          string file = string( ent->d_name );
          Mat image = imread( ( d + file ), CV_LOAD_IMAGE_COLOR );
        
          if( !image.data )
            {
              cerr <<  "No image data for " << ent->d_name << endl;
            }
          else
            {
              images->push_back( image );
              num++;
            }
        }
    
      closedir (dir);
      return num;
  
    } else {
  
      /* could not open directory */
      cerr << "Houston, we had a problem." << endl;
      return 0;
  
    }
  #else
    struct dirent **namelist;
    int num = 0;
    int n;
    string d = string( base );
    cout << "Starting to read from " << base << endl;

    n = scandir(base, &namelist, NULL, alphasort);
    if (n <= 0)
        cerr << "Could not read from directory " << base << endl;
    else {
        int i = 0;
        while (i < n) {
            cout << "Loading file   " << namelist[i]->d_name  << " i = " << i<< endl;
            string file = string( namelist[i]->d_name );
            Mat image = imread( ( d + file ), CV_LOAD_IMAGE_COLOR );
        
            if( !image.data )
              {
                cout <<  "No image data for " << file << endl;
              }
            else
              {
                images->push_back( image );
                cout <<  "Loaded " << file << endl;
                num++;
              }
            free(namelist[i]);
	         i++;
        }
        free(namelist);
        return num;
    }
     
  #endif
  
  }
  int processVideoMatch(VideoCapture& capture, vector<Mat> &templ)
  {
      Mat frame;
      char filename[200];
      int n=0;

     
      string raw_window = "Raw Video | q or esc to quit";
      
      cout << "Press space to save a picture. q or esc to quit" << endl;
      setup(templ);
      while(true){
         capture >> frame;
         if (frame.empty())
            break;
         process(frame);
         
         //delay N millis, usually long enough to display and capture input
         char key = (char) waitKey(5);
         switch (key)
         {
            case 'q':
            case 'Q':
            case 27: //escape key
               return 0;
            case ' ': //Save an image
               sprintf(filename, "filename%.3d.jpg", n++);
               imwrite(filename, frame);
               cout << "Saved " << filename << endl;
               break;
            default:
               break;
         }
      }
      return(0);
  }


  int processImageMatch(const char * dirname, vector<Mat> &templ)
    {

      cout << "Press space to save a picture. q or esc to quit" << endl;
      cout << "Any other key advances the image sequence." << endl;

      Mat frame;
      vector<Mat> sequence;
      char filename[200];
      int n=0;
      //create matcher
      setup(templ);

      getImages(dirname, &sequence);
      
      if (sequence.size() == 0){
         cerr << "No images were found in " << dirname << endl;
         return 0;
      }

      int curImage = 0;

      while(curImage < sequence.size()){

         frame = sequence[curImage];
         if (frame.empty()){
            curImage++;
            continue;
         }
         process(frame);
         
         char key = (char) waitKey(0);
         //cout << "Key pressed " << (int)(key) << endl;
         //cout << "Current image " << curImage << endl;
         switch (key){
            case 'q':
            case 'Q':
            case 27: //escape key
               //case 1048603:
               return 0;
            case ' ': //Save an image
               //case 1048608:
               sprintf(filename, "filename%.3d.jpg", n++);
               imwrite(filename, frame);
               cout << "Saved " << filename << endl;
               break;
            case 82://1113937: // left arrow
               curImage = max(0,curImage - 1);
               break;
               //case 1113939: // rightarrow
            default:
               // Advance the image sequence
               curImage++;
               break;
         }
      }
      return(0);
    }

}

int main(int ac, char** av)
{
  // call init
  initDetectorExtractor();
   
  //if (ac < 4 || av[1][0] != '-')
  // {
  //    help(av);
  //    return 1;
  // }

  vector<Mat> templ;
  string templ_name;
  string mov_name;

  // the user must specify an option argument.
  // case 1: option -v (video stream match. Default)
  // case 2: option -d (directory of images match)
  for(int i=3;i<ac;i++){
     Mat temp = imread(av[i]);
     if (!temp.data)
     {
        cerr << "Failed to read image " << av[2] << ". Exiting." << endl;
        return 1;
     }else{
        temp_names.push_back(av[i]);
         templ.push_back(temp);
     }
  }
  switch (av[1][1])
    {
      case 'd':
          
          return processImageMatch(av[2],templ);
        // no need to break

      case 'v':
      default:
        {

        mov_name = av[2];
        templ_name = av[3];
        //try to open string, this will attempt to open it as a video file
        VideoCapture capture(mov_name);
        //if this fails, try to open as a video camera, through the use of an integer param
        if (!capture.isOpened())
          {
            cerr << "Failed to open " << mov_name << endl;
            cerr << "Opening video stream " << atoi(mov_name.c_str()) << endl;
            capture.open(atoi(mov_name.c_str()));
          }
      #ifdef USE_WEBCAM
        if (!capture.isOpened())
          {
            cerr << "Failed to open a video device or video file!\n" << endl;
            help(av);
            return 1;
          }
      #endif

        return processVideoMatch(capture,templ);

        // no need to break
        }
    }
}
