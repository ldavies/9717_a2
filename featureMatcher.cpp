
//Feature extraction and matching class

#include "featureMatcher.hpp"

//global variables
cv::Ptr<FeatureDetector> _detector;
cv::Ptr<DescriptorExtractor> _extractor;

/*


        "SIFT" – SIFT
        "SURF" – SURF
        "ORB" – ORB
        "BRISK" – BRISK
        "BRIEF" – BriefDescriptorExtractor

*/

void initDetectorExtractor() {
  cv::initModule_nonfree();

  // define detector and extractor
  _detector = FeatureDetector::create("HARRIS");
  _extractor = DescriptorExtractor::create("SIFT");
  if (!_extractor || _extractor == NULL)
    {
      cerr << "Extractor not instantiated" << endl;
      exit(0);
    }
  if (!_detector || _detector == NULL)
    {
      cerr << "Detector not instantiated" << endl;
      exit(0);
    }
  else
    cout << "Detector is " << _detector << endl;
}



//constructor, also where the item to be match is processed//
featureMatcher::featureMatcher(){


}

featureMatcher::~featureMatcher(){
  _detector.release();
  _extractor.release();
  delete _templ;
}

void featureMatcher::setTemplate(Mat& t) {
  _templ = new TemplateImage(t, this); // calls computeData on this object
}

void featureMatcher::assertValid() { 
  if (_templ == NULL)// && _extractor != NULL && _detector != NULL))
    throw cv::Exception(1,string("Feature extractor not valid."),NULL,NULL, 0);
}

void computeData(Mat& t, Mat& data, vector< KeyPoint >& keypoints)
  {

    Mat processed;
    Mat descriptor;
    preProcess(t,processed);
    getKeyPoints(processed,keypoints);
    _extractor->compute(processed, keypoints, descriptor);
  
    if(descriptor.type()!=CV_32F) 
      {
        descriptor.convertTo(data, CV_32F); // make sure it's CV_32F
      }
    else 
      {
        descriptor.copyTo(data);
      }
  }


void getKeyPoints(Mat &input,  vector<KeyPoint>& output){

#ifdef USE_ORB_DETECTOR
  // feature detector
  vector< KeyPoint > test;

  _detector->detect( input, test );
  output = test;
#else
  vector<Point2f> corners_p;
  // goodFeatureToTrack
  goodFeaturesToTrack( input, corners_p, 1000, 0.01, 5, noArray(), 5, true, 0.04 );

  KeyPoint::convert(corners_p,*output);

#endif
}


//preProcess steps used by other function//
void preProcess(Mat &input, Mat &output){
   cvtColor( input, output, CV_RGB2GRAY );
   blur( output, output, Size(3,3) );
   Laplacian( output,output,CV_8U );
}

void featureMatcher::setFrame(Mat &input){
    assertValid();

   _scene = input;
   _allSceneKeypoints.clear();
   _good_matches.clear();
   preProcess(_scene,_sceneProcessed);

   vector<Rect> rois;
   _templ->_roiExtractor->extract(_scene, rois);

   for (int i=0; i< rois.size(); i++)
     {
       if (rois[i].width >= 5 && rois[i].height >= 5)
         {
           try
             {
               frameProcess(rois[i]);
             }
           catch (cv::Exception e)
             {
               cerr << "Failed to process roi " << i << endl << "Error: " << e.what() << endl;
             }
         }
     }
}


//things to do every frame is placed here//
void featureMatcher::frameProcess(Rect roi){
   assertValid();

   Mat processed = _sceneProcessed(roi);
   vector<KeyPoint> sceneKeypoints;
   Mat sceneDescriptor;
   Mat sceneData;

   getKeyPoints(processed,sceneKeypoints);

   if (sceneKeypoints.size() == 0) return;

   Point2f topleft(roi.x,roi.y);
   for (int i=0; i< sceneKeypoints.size(); i++) sceneKeypoints[i].pt += topleft;
   
   // computing _descriptors
   _extractor->compute(_sceneProcessed, sceneKeypoints, sceneDescriptor);

   if (sceneDescriptor.rows < 2) return;
   
   // convert the decriptors to be used in FLANN
   if(sceneDescriptor.type()!=CV_32F) {
      sceneDescriptor.convertTo(sceneData, CV_32F); // make sure it's CV_32F
   }
   else {
      sceneData = sceneDescriptor;
   }

   // for all pyramids in the template, test for a match.
   matchObjectScene(_templ->_original, sceneData, sceneKeypoints);
   for (int i=0; i<_templ->_pyramids.size(); i++)
     {
       matchObjectScene(&(_templ->_pyramids[i]), sceneData, sceneKeypoints);
     }

}
   
void featureMatcher::matchObjectScene(Pyramid * object, Mat& sceneData, vector< KeyPoint >& sceneKeypoints) {

   FlannBasedMatcher matcher;
   vector< vector< DMatch > > k_matches;
   int k=2;

   matcher.knnMatch(object->_data, sceneData, k_matches, k);
   
   for( int i = 0; i < k_matches.size(); i++ )
   {
      if (k_matches[i].size() >= 2)
      {
         double ratio = k_matches[i][0].distance / k_matches[i][1].distance;
         if ( ratio < KNN_RATIO )
         {
	    _allSceneKeypoints.push_back (sceneKeypoints[k_matches[i][0].trainIdx]) ;
	    k_matches[i][0].trainIdx = _allSceneKeypoints.size()-1;
	    k_matches[i][0].queryIdx += object->_kpIndex;
            _good_matches.push_back( k_matches[i][0] );
         }
      }
   }
   
}

void featureMatcher::drawMatchFrame(string name)
  {
    vector< KeyPoint > _objectKeypoints = _templ->_allKeypoints;
    Mat                _objectImage     = _templ->_original->_image;
    vector< DMatch > non_homo_matches, homo_matches;

     //-- Localize the object
     std::vector<Point2f> obj;
     std::vector<Point2f> scene;
     
     for( int i = 0; i < _good_matches.size(); i++ )
     {
        if (_good_matches[i].distance <= MAX_HOMOGRAPHY_DISTANCE)
	{
          //-- Get the keypoints from the good matches
	  homo_matches.push_back(_good_matches[i]);
          obj.push_back( _objectKeypoints[ _good_matches[i].queryIdx ].pt );
          scene.push_back( _allSceneKeypoints[ _good_matches[i].trainIdx ].pt );
	}
	else non_homo_matches.push_back(_good_matches[i]);
     }


    drawMatches(_objectImage, _objectKeypoints, _scene, _allSceneKeypoints, homo_matches, _match_frame, Scalar(0,255,0),Scalar(255,0,0));
    //drawMatches(_objectImage, _objectKeypoints, _scene, _allSceneKeypoints, non_homo_matches, _match_frame, Scalar(0,255,0),Scalar(255,0,0));
     if(obj.size() >= 4){
        Mat H = findHomography( obj, scene, CV_RANSAC );
        std::vector<Point2f> obj_corners(4);
        obj_corners[0] = cvPoint(0,0);
        obj_corners[1] = cvPoint( _objectImage.cols, 0 );
        obj_corners[2] = cvPoint( _objectImage.cols, _objectImage.rows );
        obj_corners[3] = cvPoint( 0, _objectImage.rows );
        std::vector<Point2f> scene_corners(4);
        
        perspectiveTransform( obj_corners, scene_corners, H);
        
        //-- Draw lines between the corners (the mapped object in the scene - image_2 )
        line( _match_frame, scene_corners[0] + Point2f( _objectImage.cols, 0), scene_corners[1] + Point2f( _objectImage.cols, 0), Scalar( 0, 255, 0), 4 );
        line( _match_frame, scene_corners[1] + Point2f( _objectImage.cols, 0), scene_corners[2] + Point2f( _objectImage.cols, 0), Scalar( 0, 255, 0), 4 );
        line( _match_frame, scene_corners[2] + Point2f( _objectImage.cols, 0), scene_corners[3] + Point2f( _objectImage.cols, 0), Scalar( 0, 255, 0), 4 );
        line( _match_frame, scene_corners[3] + Point2f( _objectImage.cols, 0), scene_corners[0] + Point2f( _objectImage.cols, 0), Scalar( 0, 255, 0), 4 );

     }

     //imshow("homography",_scene);
    imshow((name + "matcher").c_str(),_match_frame);
  }
