COMP9517 Assignment 2
=====================

Contributors:

 * John Lam
 * Laurence Davies

Compilation
-----------

 1. Create two directories: `assets/` and `bin/`
 2. `cd bin/` and run `cmake ..`
 3. run `make`

Usage
-----

Assuming the above compilation and assuming the samples provided reside in assets, run:

`./a2 -v ../assets/clip_sample.m4v ../assets/poster.png`

or if OpenCV has been compiled to use CUDA on a mobile GPU, prefix with `optirun` ala:

`optirun ./a2 -v ../assets/clip_sample.m4v ../assets/poster.png`

To run on a sequence of images in a directory, run:

`./a2 -d ../assets/trafficsign_a/ ../assets/trafficsign_a.png`


To run multiple template just append templates, run:

`./a2 -d ../assets/trafficsign_a/ ../assets/trafficsign_a.png ../assets/trafficsign_a_temp2.png ../assets/trafficsign_a_temp3.png `

